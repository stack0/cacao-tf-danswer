# Introduction

This terraform is an initial implementation to provision [Danswer](https://github.com/danswer-ai/danswer) in an OpenStack cloud, namely using CACAO on Jetstream2. It utilizes CACAO's base template called single-image-app-proxy. The single-image-app-proxy is a simple nginx reverse proxy that can be used to protect an application with basic authentication.

# TODO

- [ ] create a new image that uses cuda 12.2 and ollama to be deployed to js2. See [this](https://github.com/ollama/ollama/issues/732) and [this](https://github.com/ollama/ollama/blob/92578798bb1abcedd6bc99479d804f32d9ee2f6c/Dockerfile#L17-L23)